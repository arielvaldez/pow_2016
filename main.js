$(document).ready(function() {

    function user_options() {
        var left = $("#topbar__user").offset().left;
        $("#user_options").css({"left": left - 55});
    }

    function menu_viaje() {
        var left = $("#topbar__viaje").offset().left;
        $("#menu_viaje").css({"left": left - 55});   
    }

    $("#topbar__user").on("click", function() {
        user_options();
        $("#user_options").toggle();
    });

    $("#topbar__viaje").on("click", function() {
        menu_viaje();
        $("#menu_viaje").toggle();
    });

    $(window).resize(function() {
        user_options();
    });

    $(document).click(function(e) {
        if (e.target.id != "topbar__user") {
            $("#user_options").hide();
        }

        if (e.target.id != "topbar__viaje") {
            $("#menu_viaje").hide();   
        }
    });
});
