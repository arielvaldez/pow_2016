document.addEventListener("DOMContentLoaded", function() {

    setMapHeight();

    function setMapHeight() {
        var form_height = document.getElementById("form").offsetHeight;
        var remaining_height = parseInt(document.body.clientHeight - form_height - 20);
        document.getElementById("map").style.height = remaining_height + "px";
    }

    window.addEventListener("resize", setMapHeight);  // cambia con la altura de la ventana
});
